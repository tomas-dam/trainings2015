std::cin - is a predefined object and an istream class instance and is said to
           be attached to the standard input device,
           which usually is the keyboard.
           This is for reading value from keyboard in variable.

std::cout - is a predefined object and an ostream class instance and is said to
            be connected to the standard output device, which usually is the 
            display screen.
            This is for outputing message in the display screen.
