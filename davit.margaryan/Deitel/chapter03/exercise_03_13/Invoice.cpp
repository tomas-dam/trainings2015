/// Invoice.cpp
/// Implimentetion of Invoice class

#include "Invoice.hpp" /// includes Invoice definition
#include <iostream> /// allows program to perform input and output

/// consturctor initialize Invoice object's data-members
Invoice::Invoice(std:: string number, std::string description,
    int quantity, int price)
{
    setNumber(number); /// validate and store number
    setDescription(description); /// validate and store description
    setQuantity(quantity); /// validate and store quantity
    setPrice(price); /// validate and store price
} /// end Invoice constructor

/// validate and initialize number_
void
Invoice::setNumber(std::string number)
{
    number_ = number;
} /// end function setNumber

/// function to get the Invoice number
std::string
Invoice::getNumber()
{
    return number_; /// return object's number
} /// end function getNumber

/// validate and initialize description_
void
Invoice::setDescription(std::string description)
{
    description_ = description;
} /// end function setDescription

/// function to get the Invouce description
std::string
Invoice::getDescription()
{
    return description_; /// return object's description
} /// end function getDescription

/// validate and initialize quantity_
void
Invoice::setQuantity(int quantity)
{
    if(quantity < 0) { /// if quantity smaller 0
        std::cout << "Quantity can not be smaller 0!\n"; /// error message
        quantity_ = 0;
    }
    if(quantity >= 0) { /// if quantity is valid
        quantity_ = quantity;
    } /// end if
} /// end function setQuantity

/// function to get the Invoice quantity
int
Invoice::getQuantity()
{
    return quantity_; /// return object's quantity
} /// end function getQuantity

/// validate and initialize price_
void
Invoice::setPrice(int price)
{
    if(price < 0) { /// if price smaller 0
        std::cout << "Price can not be smaller 0. Reset price 0.\n"; /// error message
        price_ = 0;
    } /// end if
    if(price >= 0) { /// if price is valid
        price_ = price;
    } /// end if
} /// end function setPrice

/// function to get the Invoice price
int
Invoice::getPrice()
{
    return price_; /// return object's price
} /// end function getPrice

/// function to get the Invoice Amounth
int
Invoice::getInvoiceAmount()
{
    return getPrice() * getQuantity();
} /// end function  getInvoiceAmounth
