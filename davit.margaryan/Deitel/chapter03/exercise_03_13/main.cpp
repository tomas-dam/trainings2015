/// main.cpp
/// test class Invoice

#include "Invoice.hpp" /// include Date class definition
#include <iostream> /// allows program to perform input and ooutput

/// function main begins program execution
int
main()
{
    /// intialize Invoice object and test
    Invoice invoice("#121212", "Detal1", 12, 55);
    std::cout << "Number: " << invoice.getNumber()
              << "\nDescription: " << invoice.getDescription()
              << "\nQuantity: " << invoice.getQuantity()
              << "\nPrice: " << invoice.getPrice() 
              << "\nAmount: " << invoice.getInvoiceAmount() << std::endl; 
    return 0; /// program ends successfully
} /// end function main
