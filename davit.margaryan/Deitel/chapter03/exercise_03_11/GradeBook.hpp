#ifndef __GRADE_BOOK_HPP__
#define __GRADE_BOOK_HPP__

/// GradeBook.hpp
/// GradeBook class definition.
/// the public interface of the class.

#include <string> /// program uses standart C++ string class

/// GradeBook class defintion
class GradeBook {
public:
    /// constructor initialize data members
    GradeBook(std::string courseName, std::string instructorName);
    void setCourseName(std::string courseName); /// sets the coureName_
    std::string getCourseName(); /// gets the courseName_
    void setInstructorName(std::string instructorName); /// sets the instructorName_
    std::string getInstructorName(); /// gets the instructorName_
    void displayMessage(); /// display message
private:
    std::string courseName_;
    std::string instructorName_;
}; /// end class gradebook

#endif /// __GRADE_BOOK_HPP__ 
