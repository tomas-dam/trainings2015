/// Date.cpp
/// Implimentetions of the Date class member-function defintions

#include "Date.hpp" /// include definition of class Date
#include <iostream> /// allows program to perform input and output

/// constructor initializes object's data members
Date::Date(int month, int day, int year)
{
    setMonth(month); /// validate and store month_
    setDay(day); /// validare and store day_
    setYear(year); /// validate and store year_
} /// end constructor

/// function that sets the month
void
Date::setMonth(int month)
{
    if(month > 12 || month <= 0) { /// if invalid month
        std::cout << "Invalid month! Reset month to 1.\n"; /// error message
        month_ = 1;
    } /// end if
    if(month > 0 && month <= 12) { /// if valid month
        month_ = month;
    } /// end if
} /// end function setMonth

/// function to get the month
int
Date::getMonth()
{
    return month_; /// return object's month
} /// end function getMounth

/// function that sets the day
void
Date::setDay(int day)
{
    day_ = day;
} /// end function setDay

/// function to get day
int
Date::getDay()
{
    return day_; /// return object's day
} /// end function getDay

/// function that sets the Year
void
Date::setYear(int year)
{
    year_ = year;
} /// end function setYear

/// function to get year
int
Date::getYear()
{
    return year_; /// return object's yaer
} /// end function getYear

/// function to print the date
void
Date::displayDate()
{
    std::cout << getMonth() << "/" << getDay() << "/" << getYear();
} /// end function displayDate
