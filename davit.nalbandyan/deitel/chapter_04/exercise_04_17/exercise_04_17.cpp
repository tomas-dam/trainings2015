#include <iostream>

int 
main()
{
    int counter = 0;
    double largest, number;
    std::cout << "\nInput number " << counter << ": " ;
    std::cin >> number;
    largest = number;
    ++counter;
    while (counter < 10) {
        std::cout << "\nInput number " << counter << ": " ;
        std::cin >> number;
        if (number > largest) {
            largest = number;
        }
        ++counter;
    }
    std::cout << "\nThe largest number is: " << largest << std::endl;
    return 0;
}
