#include<iostream>

int 
main()
{
    int a;
    std::cout << "Enter a number: ";
    std::cin >> a;

    if (a % 2 == 0)
        std::cout << a << " Even number\n";
    if (a % 2 != 0)
        std::cout << a << " Odd number\n";

    return 0;
}
