#include <iostream>

int 
main()
{
    int x, y, z;
    int sum, average, product, max, min;

    std::cout << "Enter numbers \n";
    std::cin  >> x >> y >> z;

    sum = x + y + z;
    average = (x + y + z) / 3;
    product = x * y * z;

    std::cout << "Sum " << sum << "\n";
    std::cout << "Average " << average << "\n";
    std::cout << "Product " << product << "\n";
   
    max = x, min = x;

    if (y > max )
        max = y;          
    if(z > max)
        max = z;

    if (y < min)
        min = y;
    if (z < min)
        min = z;   
  
    std::cout << max << " is Biggest " << std::endl;
    std::cout << min << " is Smallest " << std::endl;
 
    return 0;
}
