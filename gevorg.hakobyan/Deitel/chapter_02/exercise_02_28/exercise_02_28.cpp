#include <iostream>

int 
main()
{
    int x, a, b, c, d, e;

    std::cout << "Enter five-digit number: ";
    std::cin >>  x ;

    if (x <= 99999) {
        if (x >=10000 ) {

            a = x / 10000;
			
            b = (x / 1000) % 10;

            c = (x / 100) % 10;

            d = (x / 10) % 10;

            e = x % 10;

            std::cout << a << "   " << b << "   " << c << "   " << d << "   " << e << std::endl;
        }
    }
		    
    return 0;
}

