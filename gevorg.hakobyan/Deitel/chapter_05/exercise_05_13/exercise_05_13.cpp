#include <iostream>

int
main()
{
    for (int counter = 1; counter <= 5; ++counter){
        int count;
        std::cout << "Enter the count of stars: ";
        std::cin  >> count;

        for (int counter = 1; counter <= count; ++counter){
            std::cout << "*";
        }
        std::cout << std::endl;
    }

    return 0;
}
