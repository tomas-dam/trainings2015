#include <iostream>
#include <iomanip>

int
main()
{ 
    std::cout << "Decimal" << std::setw(18) << "Binary" << std::setw(18) << "Octal" << std::setw(18) << "Hex" << std::endl;
   
    for (int number = 1; number <= 256; ++number){
        int mul = 1, binary = 0, number1 = number;
        while (number1 > 0){
            int binaryDigit = number1 % 2;
            binary += mul * binaryDigit;
            mul *= 10;
            number1 /= 2;
        }
        std::cout << std::dec << number << std::setw(20) << binary;
        std::cout << std::setw(20) << std::oct << number;
        std::cout << std::setw(20) << std::hex << number << std::endl;
    }

    return 0;
}
