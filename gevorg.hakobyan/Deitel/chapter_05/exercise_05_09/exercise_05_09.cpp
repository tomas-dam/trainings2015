#include <iostream>

int
main()
{
    int composition = 1;
    for (int number = 1; number <= 15; number += 2){
        composition *= number;
    }

    std::cout << "The composition of odd number at 1 to 15 is " << composition << std::endl;

    return 0;
}
