#include <iostream>

int
main()
{
    int number;

    std::cout << "Enter the five-digit number: ";
    std::cin  >> number;

    if (number < 10000){
        std::cout << "Error 1: The number should be five-digit.\nTry again." << std::endl;
        return 1;
    }
    if (number > 99999){
        std::cout << "Error 1: The number should be five-digit.\nTry again." << std::endl;
        return 1;
    }

    int digit1 = number / 10000;
    int digit2 = (number / 1000) % 10;
    int digit4 = (number / 10) % 10;
    int digit5 = number % 10;
    
    
    if (digit1 == digit5){
        if (digit2 == digit4){
            std::cout << "The number is palindrome." << std::endl;
            return 0;
        } 
    }

    std::cout << "The number isn't palindrome." << std::endl;
       
    return 0;
}
