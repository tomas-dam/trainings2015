#include <iostream>
#include <iomanip>

int 
main()
{   
    double sales;    

    std::cout << "Enter sales in dollars (non-positive number to quit): ";
    std::cin  >> sales;
    
    while (sales > 0){
        double salary = sales * 0.09 + 200;

        std::cout << "Salary: $" << std::setprecision(2) << std::fixed << salary;

        std::cout << "\n\nEnter sales in dollars (non-positive number to quit): ";
        std::cin  >> sales;
    }
    return 0;
}
