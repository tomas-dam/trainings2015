#include <iostream>

int
main()
{
    double radius;

    std::cout << "Enter the radius: ";
    std::cin  >> radius;
    
    if (radius < 0){
        std::cout << "Error 1: The radius should be non-negative.\nTry again." << std::endl;
        return 1;
    }

    double pi = 3.14159;
    double diameter = 2 * radius;
    double circumference = 2 * pi * radius;
    double square = pi * radius * radius;

    std::cout << "Diameter = " << diameter << std::endl;
    std::cout << "The circumference = " << circumference << std::endl;
    std::cout << "Square = " << square << std::endl;

    return 0;
}   
