#include <iostream>

int 
main()
{
    int  vertical = 1;
      
    while (vertical <= 8){
        int horizontal = 1;

        if (vertical % 2 == 0){
            std::cout << " ";    
        }
        while (horizontal <= 8){
            std::cout << "* ";
            ++horizontal;                 
        }
        std::cout << std::endl;
        ++vertical;
    }
   
    return 0;
}
