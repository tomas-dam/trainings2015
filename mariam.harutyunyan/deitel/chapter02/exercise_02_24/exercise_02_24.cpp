#include <iostream>

int
main()
{
    int number;
    std::cout << "Enter number: ";
    std::cin >> number;

    if (number % 2 == 0) {
        std::cout << "The number is even " << std::endl;
    }
    if (number % 2 != 0) {
        std::cout << "The number is odd " << std::endl;
    }
    
    return 0;
}
