#include <iostream>

int
main()
{
    int number1, number2;

    std::cout << "Enter the first number: ";    
    std::cin  >> number1;
    std::cout << "Enter the second number: ";
    std::cin  >> number2; 

    if (0 == number1){
        std::cout << "Error 1: The first number can't be zero" << std::endl;
        return 1;    
    }    

    if (0 == number2 % number1){
        std::cout << "The second number is divided to the first" << std::endl;
        return 0;    
    }    
      
    std::cout << "The second number is not devided to the first" << std::endl;
      
    return 0;
}
