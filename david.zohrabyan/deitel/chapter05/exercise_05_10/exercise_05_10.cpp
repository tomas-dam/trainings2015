#include <iostream>
#include <iomanip>

int
main()
{
    int number = 1;
    int factorial = 1;
    std::cout << "Number" << std::setw(21) << "Factorial of number" << std::endl;
    while(number <= 5) {
        factorial *= number;
        std::cout << std::setw(5) << number << "!" << std::setw(21)<< factorial << std::endl;
        ++number; 
    }

    return 0;
}

/* as the variable has the int type, programs can't print number of a factorial 20 because the operator int has range less
  - 2 147 483 648/2 147 483 647.*/
