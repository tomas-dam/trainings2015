#include <iostream>

int
main()
{
    std::cout << "This program does one of the followings: \n";
    std::cout << "1. Calculate factorial \n";
    std::cout << "2. Calculate Euler's number \n";
    std::cout << "3. Calculate x power of Euler's number \n" << std::endl;
    
    int choice;
    std::cout << "Enter choice: "; 
    std::cin >> choice; 

    if (1 == choice) {
        int number;
        std::cout << "Enter positive number to calculate it`s factorial: ";	
        std::cin >> number;
        int factorial = number;  
        if (number > 0) {
            while(1 != number) {
                --number;
                factorial *= number;
            } 
            std::cout << "The factorial is " << factorial << std::endl;
        } else {
            std::cerr << "Error 1. Entered number should be none-zero positive" << std::endl;
            return 1;
        }

    } else if (2 == choice) {
        double eulersNumber = 1;
        int factorial = 1, counter = 1, accuracy;
        std::cout << "Please enter accuracy: ";
        std::cin >> accuracy;
        while (accuracy >= counter) {
            factorial *= counter;
            eulersNumber += 1.0 / factorial ;
            ++counter;
        }
        std::cout << "Euler`s number is " << eulersNumber << std::endl;
 	
    } else if (3 == choice) {
        double eulerDegree = 1, xpow = 1, x;
        int factorial = 1, counter = 1, accuracy;
        std::cout << "Enter x to calculate x power of Euler`s number: ";
        std::cin >> x;
 
        std::cout << "Enter necessary accuracy: ";
        std::cin >> accuracy;

        while (accuracy >= counter) {
            factorial *= counter;
            xpow *= x; 
            eulerDegree += xpow / factorial;
            ++counter;
        }
        std::cout << "The x power of Euler`s number is " << eulerDegree  << std::endl; 	
    } else {
        std::cerr << "Error 2. Invalid choice - either 1, 2, or 3 should be chosen." << std::endl;
        return 2;
    }
 	
    return 0;
}
