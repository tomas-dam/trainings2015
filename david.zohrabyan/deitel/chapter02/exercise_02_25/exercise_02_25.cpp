/// This program reads out two integers,defines and prints,whether the first is multiple to the second
#include <iostream> /// standard input output

/// function main begins program execution 
int
main()
{
    int a, b; /// announcement of variables

    std::cout << "Enter first variable: " << std::endl; /// prompt user for data
    std::cin >> a; /// reads number from user into "a"

    std::cout << "Enter second variable: " << std::endl; /// prompt user for data
    std::cin >> b; /// reads number from user into "b"

    if (b != 0) { /// compare variable "b" with zero
        if (a % b == 0) /// solves on the module,the division rest is equal to zero or not
            std::cout << "The first is multiple to the second: " << std::endl; /// output the text;end line

        if (a % b != 0) /// solves on the module,the division rest is equal to zero or not
            std::cout << "The first isn`t multiple to the second: " << std::endl; /// output the text;end line
    }
    if (b == 0) /// compare variable "b" with zero
        std::cout << "Eror. It`s impossible to divide number into zero: " << std::endl; /// notifies the user on mistake
    
    return 0; /// program ended successfully
} /// end function main

