/// This program accepts two numbers,then prints the biggest number after the word "biggest is " or if numbers are equal ,prints "these numbers are equal 
#include <iostream> /// standard input output

/// function main begins program execution
int
main()
{
    int a, b; /// announcement of variables

    std::cout << "Enter first number: " << std::endl; /// prompt user for data
    std::cin >> a ; /// read number from user into "a"

    std::cout << "Enter second number: " << std::endl; /// promt user for data
    std::cin >> b ; /// read number from user into "b" 

    if (a > b) /// compares number of "a" to number "b"
       std::cout << "The biggest is: " << a << std::endl; /// display number "a";end line
 
    if (b > a) /// compares number of "b" to number "a"
       std::cout << "The biggest is: " << b << std::endl; /// display number "b";end line

    if (a == b) /// compares numbers "a" and "b"
       std::cout << "These numbers are equal " << std::endl; /// output the text;end line
 
    return 0; /// program ended successfully
} /// end function main
