///The  heading file of GradeBook  
#include <string> ///standard string type

///Represents a layer of the process
class GradeBook
{
public:
    ///Constructor initializing courseName and teacherName
    GradeBook(std::string name, std::string teacher);
    
    ///set courseName
    void setCourseName(std::string name);
    
    ///getting name of the course
    std::string getCourseName();
    
    ///set teacherName
    void setTeacherName(std::string teacher);
    
    ///getting name of the course
    std::string getTeacherName();
    
    void displayMessage();
    
    ///the name of course nad teacher for this GradeBook
private:
    std::string courseName_;
    std::string teacherName_;
};
    
