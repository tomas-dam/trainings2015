/// The heading file of Employee
#include <string> /// standard string type

class Employee
{
public:
    /// Constructor initializing
    Employee(std::string name, std::string surname, int salary);
    
    /// function set Name
    void setName(std::string name);
    
    /// function set SurName
    void setSurName(std::string surname);
    
    /// function set Salary
    void setSalary(int salary);
    
    /// function get Name
    std::string getName();
    
    /// function get SurName
    std::string getSurName();
    
    /// function get Salary
    int getSalary();
    
    /// function get AnnualWage
    int getAnnualWage();
    
    /// function get SalaryPersent
    void changeSalaryByPercent(int percent);
    
private:
    /// member variable of Name
    std::string name_;
    
    /// member variable of SurName
    std::string surname_;
    
    /// member variable of Salary
    int salary_;
 
};
